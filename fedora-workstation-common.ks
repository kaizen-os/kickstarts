%packages

# Exclude unwanted groups that fedora-live-base.ks pulls in
-@dial-up
-@input-methods
-@standard

# Make sure to sync any additions / removals done here with
# workstation-product-environment in comps
@base-x
@core
@guest-desktop-agents
@hardware-support
@multimedia
@networkmanager-submodules
@printing

# Remove LibreOffice
-@libreoffice
-libreoffice*

# Remove Fedora Branding
-fedora-productimg-workstation
-fedora-release
-fedora-release-notes
-fedora-release-workstation
-fedora-logos

# Remove SELinux Troubleshooter desktop file
-setroubleshoot

# Remove Problem Reporting desktop file
-abrt-desktop

# There's no generic equivalent of this
#-fedora-bookmarks

# Exclude unwanted packages from @anaconda-tools group
-gfs2-utils
-reiserfs-utils

# Add useful CLI text editors
nano
%end
