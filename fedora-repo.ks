# Include the appropriate repo definitions

# Exactly one of the following should be uncommented

# For non-master branches the following should be uncommented
%include fedora-repo-not-rawhide.ks
%include rpmfusion-repo-not-rawhide.ks

# Include Pantheon Desktop stable release packages
#%include pantheon-stable-repo.ks

# Include Pantheon Desktop nightly build packages
# %include pantheon-nightly-repo.ks

# Include Kaizen nightly repo
%include kaizen-nightly-repo.ks
