# Maintained by the Fedora Workstation WG:
# http://fedoraproject.org/wiki/Workstation
# mailto:desktop@lists.fedoraproject.org

%include fedora-live-base.ks
%include fedora-workstation-common.ks
#
# Disable this for now as packagekit is causing compose failures
# by leaving a gpg-agent around holding /dev/null open.
#
#include snippets/packagekit-cached-metadata.ks

# We don't want to use the gnome comps group
%include fedora-gnome-group-subset.ks

# We don't want to use the workstation-product comps group
%include fedora-workstation-product-group-subset.ks

# Add Kaizen Desktop stuff
%include kaizen-desktop-packages.ks
part / --fstype="ext4" --size 6656

%post

cat >> /etc/rc.d/init.d/livesys << EOF


# disable gnome-software automatically downloading updates
cat >> /usr/share/glib-2.0/schemas/org.gnome.software.gschema.override << FOE
[org.gnome.software]
download-updates=false
FOE

# don't autostart gnome-software session service
rm -f /etc/xdg/autostart/gnome-software-service.desktop

# disable the gnome-software shell search provider
cat >> /usr/share/gnome-shell/search-providers/org.gnome.Software-search-provider.ini << FOE
DefaultDisabled=true
FOE

# don't run gnome-initial-setup
mkdir ~liveuser/.config
touch ~liveuser/.config/gnome-initial-setup-done

# suppress anaconda spokes redundant with gnome-initial-setup
cat >> /tmp/etc/sysconfig/anaconda << FOE
[NetworkSpoke]
visited=1

[PasswordSpoke]
visited=1

[UserSpoke]
visited=1
FOE

# make the installer show up
if [ -f /usr/share/applications/liveinst.desktop ]; then
  # Show harddisk install in shell dash
  sed -i -e 's/NoDisplay=true/NoDisplay=false/' /usr/share/applications/liveinst.desktop ""
  # need to move it to anaconda.desktop to make shell happy
  mv /usr/share/applications/liveinst.desktop /usr/share/applications/anaconda.desktop

  # Make the welcome screen show up
  if [ -f /tmp/usr/share/anaconda/gnome/fedora-welcome.desktop ]; then
    mkdir -p ~liveuser/.config/autostart
    cp /usr/share/anaconda/gnome/fedora-welcome.desktop /usr/share/applications/
    cp /usr/share/anaconda/gnome/fedora-welcome.desktop ~liveuser/.config/autostart/
  fi

  # Copy Anaconda branding in place
  if [ -d /usr/share/lorax/product/usr/share/anaconda ]; then
    cp -a /usr/share/lorax/product/* /
  fi
fi

# rebuild schema cache with any overrides we installed
glib-compile-schemas /usr/share/glib-2.0/schemas

# set up lightdm autologin
sed -i 's/^#autologin-user=.*/autologin-user=liveuser/' /etc/lightdm/lightdm.conf
sed -i 's/^#autologin-user-timeout=.*/autologin-user-timeout=0/' /etc/lightdm/lightdm.conf

# set Pantheon as default session, otherwise login will fail
sed -i 's/^#user-session=.*/user-session=pantheon/' /etc/lightdm/lightdm.conf

# Turn off PackageKit-command-not-found while uninstalled
if [ -f /etc/PackageKit/CommandNotFound.conf ]; then
  sed -i -e 's/^SoftwareSourceSearch=true/SoftwareSourceSearch=false/' /etc/PackageKit/CommandNotFound.conf
fi

# make sure to set the right permissions and selinux contexts
chown -R liveuser:liveuser /home/liveuser/
restorecon -R /home/liveuser/

EOF

dnf config-manager --set-enabled google-chrome

# Write out Pantheon Desktop repo file
#cat > /etc/yum.repos.d/decathorpe-elementary-nightly.repo << EOF
#[decathorpe-elementary-nightly]
#name=Copr repo for elementary-nightly owned by decathorpe
#baseurl=https://copr-be.cloud.fedoraproject.org/results/decathorpe/elementary-nightly/fedora-\$releasever-\$basearch/
#type=rpm-md
#skip_if_unavailable=True
#gpgcheck=1
#gpgkey=https://copr-be.cloud.fedoraproject.org/results/decathorpe/elementary-nightly/pubkey.gpg
#repo_gpgcheck=0
#enabled=1
#enabled_metadata=1
#EOF

# Write out Kaizen nightly repo file
cat > /etc/yum.repos.d/codygarver-kaizen.repo << EOF
[codygarver-kaizen]
name=Copr repo for Kaizen nightly owned by codygarver
baseurl=https://copr-be.cloud.fedoraproject.org/results/codygarver/kaizen/fedora-\$releasever-\$basearch/
type=rpm-md
skip_if_unavailable=True
gpgcheck=1
gpgkey=https://copr-be.cloud.fedoraproject.org/results/codygarver/kaizen/pubkey.gpg
repo_gpgcheck=0
enabled=1
enabled_metadata=1
EOF

%end
