%packages
# Because there's no comps group uploaded into the copr repo,
# we're specifying the packages manually here

# applications
appcenter
contractor
elementary-calculator
elementary-calendar
elementary-camera
elementary-capnet-assist
elementary-code
# elementary-dpms-helper isn't available yet
#elementary-dpms-helper
elementary-files
# elementary Mail isn't available yet
#elementary-mail
elementary-music
elementary-photos
elementary-print
elementary-screenshot-tool
elementary-terminal
elementary-videos
firefox
PackageKit
pantheon-agent-geoclue2
pantheon-agent-polkit
switchboard
switchboard-plug-*

# artwork
elementary-icon-theme
elementary-theme
elementary-theme-gtk2
elementary-theme-gtk3
elementary-theme-plank
elementary-wallpapers
kaizen-logos
kaizen-wallpapers
kaizen-theme-plank-dark

# config
kaizen-default-settings
kaizen-release
kaizen-release-notes
pantheon-session-settings

# desktop environment
gala
plank
wingpanel
wingpanel-applications-menu
wingpanel-indicator-*

# fonts
google-roboto-condensed-fonts
google-roboto-fonts
google-roboto-mono-fonts
google-roboto-slab-fonts
open-sans-fonts

# login
elementary-greeter
light-locker
lightdm

# misc
fedora-workstation-repositories
gnome-system-monitor
htop
vim-enhanced

# FIXME: only include on live sessions
gparted

%end
