# Kaizen Kickstarts


[![build status](https://gitlab.com/PantheonOS/fedora-pantheon-ks/badges/f26/build.svg)](https://gitlab.com/PantheonOS/fedora-pantheon-ks/commits/f26)

This repository contains kickstart files forked from the Fedora Workstation kickstarts from the [fedora-kickstarts repository](https://pagure.io/fedora-kickstarts).

Should be occasionally rebased on https://pagure.io/fedora-kickstarts

```
git remote add upstream https://pagure.io/fedora-kickstarts

git fetch upstream

git rebase upstream/master
```

This collection of kickstarts can be used to build a unbranded live media ISO using Fedora 26 software and the [elementary-nightly COPR](https://copr.fedorainfracloud.org/coprs/decathorpe/elementary-nightly/).

# Getting the Live Media Creator

```bash

$ sudo dnf install anaconda lorax virt-install libvirt-daemon-config-network pykickstart

```

# Creating the flattened KS for `livemedia-creator`

Run the following command:

```bash

$ ksflatten --config fedora-live-workstation.ks -o kaizen-live-workstation.ks

```

# Creating the live media with `livemedia-creator`

```bash

$ sudo setenforce 0
$ sudo livemedia-creator --ks kaizen-live-workstation.ks --resultdir /var/tmp/lmc-f26-pantheon --no-virt --project PantheonOS-Live --make-iso --volid PantheonOS-F26 --iso-only --iso-name PantheonOS-live-f26.iso --releasever 26 --title PantheonOS-live --macboot
$ sudo setenforce 1
```
